import Foundation

struct WeatherInfo: Decodable {
    let weather: [Weather]
}

struct Weather: Decodable {
    let id:Int
    let description:String
    let icon:String
}
