//
//  ViewController.swift
//  deber01
//
//  Created by Diego Suárez on 26/6/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setImage(icon:String) {
        let iconImg: String = ("\(icon)" + ".png")
        let imageUrlString = "https://openweathermap.org/img/w/" + iconImg
        let imageUrl: URL = URL ( string: imageUrlString )!
        
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        //let imageView = UIImageView(frame: CGRect(x:0, y:0, width:200, height:200))
        //imageView.center = self.view.center
        let image = UIImage(data: imageData as Data)
        self.iconImageView.image = image
        //self.iconImageView.contentMode = UIViewContentMode.scaleAspectFit
        //self.view.addSubview(imageView)
    }

    @IBAction func requestButtonPressed(_ sender: Any) {
        let city: String = cityText.text ?? "quito"
        let key: String = "81bd160665598a6ad1c0d3fc9f1152e1"
        let urlString =  "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + key
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) {
            data, response, error in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(WeatherInfo.self, from: data) else {
                print("Error decoding Weather")
                return
            }
            
            DispatchQueue.main.async {
                self.resultLabel.text = "\(weatherInfo.weather[0].description)"
                self.setImage(icon: weatherInfo.weather[0].icon)
            }
        }
        
        task.resume()
    }
}

